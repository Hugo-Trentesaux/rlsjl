module stack

# Module allowing to automatically parse TOML info of raw file

using Mmap
using TOML

dataType = Dict(
    "uint16" => UInt16
)

# ===== structure definition =====

struct Stack{T,N} <: AbstractArray{T,N}
    file::String # path plus filename of raw raster file
    dims::NTuple{N,Int} # N integers correspondig to dimension sizes
    space::String # e.g. RAST
    m::Array{T,N} # memory mapped array
    # core constructor
    Stack(file::String, T::DataType, dims::NTuple, space::String) =
      new{T,length(dims)}(file, dims, space,
        Mmap.mmap(open(file), Array{T, length(dims)}, dims))
end

# ===== arguments getters =====

# getting arguments with matlab format
function getargsvmat(p::Dict{AbstractString,Any})
    T = dataType[p["meta"]["bytedepth"]]
    dims = (p["size"]["x"], p["size"]["y"], p["size"]["z"], p["size"]["t"])
    space = p["meta"]["space"]
    return (T, dims, space)
end

# first version for getting arguments
function getargsv0(p::Dict{AbstractString,Any})
    T = dataType[p["type"]]
    dims = p["dims"]
    space = p["space"]
    return (T, dims, space)
end

getargs = getargsvmat # /!\ using matlab version

# ===== constructors =====

# allowing to automatically parse size
function Stack(file::String)
    pathtag = replace(file, r"\.\w+$" => "") # remove extension
    return Stack(file, pathtag * ".toml")
end

# allow giving custom toml file
function Stack(file::String, toml::String)
    p = TOML.parsefile(toml)
    (T, dims, space) = getargs(p)
    tdims = ntuple(i->dims[i], length(dims))
    return Stack(file, T, tdims, space)
end

# ===== interface implementation =====

# following functions allows to access Stack like an Array
Base.size(S::Stack) = S.dims
Base.getindex(S::Stack, i::Int) = S.m[i] # linear indexing is the nature of memory storage
Base.getindex(S::Stack{T,N}, I::Vararg{Int, N}) where {T,N} = S.m[I...] # cartesian indexing

export Stack

end
