module wsviewer # module for websocket loops

using HTTP
using HTTP.IOExtras
using HTTP.Sockets

using dcimg
using stack
using JSON
using ImageMagick
using FileIO
include("./functions.jl")

function viewer(ws::HTTP.WebSockets.WebSocket, d::Dcimg)
    while !eof(ws) # infinite loop while ws is open
        try
            data = readavailable(ws) # read binary data
            jsonData = String(data) # convert data to string
            println("dcimg "*jsonData) # prints what we got
            dic = JSON.parse(jsonData) # parse string as JSON
            z = tryparse(Int, dic["z"]) # get layer
            t = tryparse(Int, dic["t"]) # get time
            min = tryparse(Int, dic["min"]) # get min
            max = tryparse(Int, dic["max"]) # get max
            pix = rescaled.(getRAframe(d, z, t), min, max) # values between 0 and 1
            pix = rotl90(pix) # rotate to get the "physisist axes"
            save(Stream(format"JPEG", ws), pix); # write binary jpeg to web socket
        catch
            # on error, do nothing TODO return error message in image
        end
    end
end

function viewer(ws::HTTP.WebSockets.WebSocket, s::Stack)
    while !eof(ws) # infinite loop while ws is open
        try
            data = readavailable(ws) # read binary data
            jsonData = String(data) # convert data to string
            println("stack "*jsonData) # prints what we got
            dic = JSON.parse(jsonData) # parse string as JSON

            request = dic["request"] # gets request type

            if request == "frame"
                z = tryparse(Int, dic["z"]) # get layer
                t = tryparse(Int, dic["t"]) # get time
                min = tryparse(Int, dic["min"]) # get min
                max = tryparse(Int, dic["max"]) # get max
                pix = rescaled.(s[:, :, z, t], min, max) # values between 0 and 1
                pix = rotl90(pix) # rotate to get the "physisist axes"
                buff = IOBuffer()
                write(buff, convert(Array{UInt8,1}, ['i'])) # magic byte telling it's an image
                save(Stream(format"JPEG", buff), pix); # write binary jpeg to web socket
                write(ws, take!(buff))
            elseif request == "slice"
                x = dic["x"] # get x
                y = dic["y"] # get y
                z = tryparse(Int, dic["z"]) # get layer
                values = s[x, y, z, :] # values
                buff = IOBuffer()
                write(buff, convert(Array{UInt8,1}, ['s'])) # magic byte telling it's a slice
                write(buff, convert(Array{UInt32,1}, values)); # write binary values as UInt32
                write(ws, take!(buff))
            end
        catch e
            # nothing to keep websocket alive
            println("error")
            println(e)
        end
    end
end


export viewer

end
