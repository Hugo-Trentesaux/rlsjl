module transform

# experimental module to change indices before accessing the matrix
# TODO generalize to any matrix
# TODO precise types for speed gain

using stack

dimensionName = Dict(
"R" => 'x', # right
"L" => 'x', # left
"A" => 'y', # anterior
"P" => 'y', # posterior
"S" => 'z', # superior
"I" => 'z', # inferior
"T" => 't', # time (forward)
)

# get position of the dimension in the mode word
# letter is one of (x,y,z,t), mode is a string array like ["R", "A", "S", "T"]
function place(letter, mode)
    for (i,l) in enumerate(mode)
        if dimensionName[l] == letter
            return i
        end
    end
    error("letter not found")
end

# get permutation and reversal for these modes
function getTransformation(inMode, outMode)
    # returns a function that executes on indices
    inMode = split(uppercase(inMode), "")
    outMode = split(uppercase(outMode), "")
    order = [place(dimensionName[dim], outMode) for dim in inMode] # list of dimension places
    if !isperm(order)
        error("not a permutation")
    end
    permute!(inMode, order)
    reversed = inMode .!= outMode
    return (order, reversed)
end

# returns the function that reverse index in position pos
function getRevFun(pos, dim)
    f! = function (x::Array{Int,1}) #::Nothing
        x[pos] = dim - x[pos] + 1
        nothing
    end
    return f!
end

# get the full transform function
function getTransformFunction(order, reversed, dims)
    # list of index reversing functions
    revfuns = [getRevFun(i, dims[i]) for (i,v) in enumerate(reversed) if v]
    revfuns! = collect(revfuns) # ::Array{Function,1}
    return function (x::Array{Int,1}) #::Nothing
        permute!(x, order) # permute
        for f! in revfuns! # reverse those who have to
            f!(x)
        end
        nothing
    end
end

# # exactly the same execution time
# # (mush slower when acessing one frame for example)
# function singleUseTrans(x::Array{Int,1})
#     permute!(x, [2, 1, 3, 4])
#     x[3] = 20-x[3]+1
#     nothing
# end

# wrapper structure for a stack
struct RASTarray{T,N} <: AbstractArray{T,N}
    stack::Stack{T,N} # base stack
    trans!::Function # transformation function on cartesian indices
    dims::Array{Int,1} # tuple representing dimension
    function RASTarray(stack::Stack{T,N}) where {T,N}
        (order, reversed) = getTransformation(stack.space, "RAST")
        dims = collect(size(stack))
        permute!(dims, order)
        trans = getTransformFunction(order, reversed, dims)
        trans = singleUseTrans
        new{T,N}(stack, trans, dims)
    end
end

# following functions allows to access RASTarray like an Array
Base.size(array::RASTarray) = tuple(array.dims...) # convert to tuple

Base.getindex(array::RASTarray, i::Int) = error("no linear indexing") # S.m[i] # linear indexing not allowed

function Base.getindex(array::RASTarray, I::Vararg{Int, 4}) # cartesian indexing
    indices = collect(I)
    array.trans!(indices)
    return array.stack[indices...]
 end

export RASTarray


# === frame part ===

function getFrameTransform(inMode, outMode) # outMode = RA
    (order, reversed) = getTransformation(inMode, outMode)
    revs = [i for (i,v) in enumerate(reversed) if v]
    return function (frame::Array{T,2}) where {T} # ::Array{T,2}
        f = permutedims(frame, order)
        for i in revs
            f = reverse(f, dims=i)
        end
        return f
    end
end

end
