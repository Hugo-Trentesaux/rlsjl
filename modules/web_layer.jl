# the goal of the web layer module is to overload constructors to create objects from a web request




# === object getters ===
# return dcimg
function Dcimg(http::HTTP.Request) # get dcimg from request
    d = HTTP.queryparams(HTTP.URI(http.target))
    return Dcimg(d["path"])
end

# return stack
function Stack(http::HTTP.Request) # get dcimg from request
    d = HTTP.queryparams(HTTP.URI(http.target))
    return Stack(d["path"])
end






# === file browser ===
# return list of current directory
function filebrowser(http::HTTP.Request)
    d = HTTP.queryparams(HTTP.URI(http.target))
    path = ""
    l = Array{String,1}()
    try
        path = d["path"]
    catch
        return JSONresponse(JSON.json("no path given"))
    end
    try
        l = readdir(path)
    catch
        return JSONresponse(JSON.json("location not found"))
    end
    dirs = []
    files = []
    hidden = []
    dcimg = []
    bin = []
    for e in l
        if e[1] == '.' # hidden file or folder
            push!(hidden, e)
        else
            if isdir(joinpath(path,e)) # if dir
                push!(dirs, e)
            else # file
                if endswith(e, ".dcimg")
                    push!(dcimg, e)
                elseif endswith(e, ".bin")
                    push!(bin, e)
                else
                    push!(files, e)
                end
            end
        end
    end
    serialized = Dict( "dirs" => dirs,
                    "files" => files,
                    "dcimg" => dcimg,
                    "bin" => bin,
                    "hidden" => hidden,)
    return JSONresponse(JSON.json(serialized))
end
