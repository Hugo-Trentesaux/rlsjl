# the goal of the serializer module is to provide JSON serialization for exposed objects

function serialize(d::Dcimg)
    if d != nothing
        serialized = Dict(
            "x"=>d.x,
            "y"=>d.y,
            "z"=>d.z,
            "t"=>d.t,
            )
    else
        serialized = "file not found"
    end
    return serialized
end

function serialize(s::Stack)
    if s != nothing
        (x,y,z,t) = size(s)
        serialized = Dict(
            "x"=>x,
            "y"=>y,
            "z"=>z,
            "t"=>t,
            )
    else
        serialized = "file not found"
    end
    return serialized
end
