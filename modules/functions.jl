function rescaled(number::UInt16, MIN, MAX)::Float64
    # rescale value between 0 and 1
    if number < MIN
        return 0.0
    elseif number > MAX
        return 1.0
    else
        return (number-MIN)/(MAX-MIN)
    end
end
