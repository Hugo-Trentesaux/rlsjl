module dcimg

# Module allowing to read data in dcimg

using Mmap
using TOML
import transform.getFrameTransform
import transform.place # only for tweak

struct Dcimg <: AbstractArray{UInt16,4} # struct allowing to access dcimg file
    filename::String # filename of the dcimg
    header::Int # header size in bytes
    clock::Int # clock size in bytes
    space::String # space
    x::Int
    y::Int
    z::Int
    t::Int
    m::Array{UInt16, 3} # linear memory map
    ftrans::Function # for a frame, the function that returns it in RA orientation
    ztrans::Function # z transform function
    function Dcimg(filename, header, clock, space, x, y, z, t)
        mmap = Mmap.mmap(open(filename), Array{UInt16, 3},
            (x*y+clock÷sizeof(UInt16), z, t), header)
        ftrans = getFrameTransform(space[1:2],"RA")
        zorder = space[3]
        if zorder == 'S' # good order
            ztrans = Z -> Z # identity
        elseif zorder == 'I' # bad order
            ztrans = Z -> z - Z +1 # reverse
        else # not implemented
            error("$zorder Not implemented")
        end
        return new(filename, header, clock, space, x, y, z, t, mmap, ftrans, ztrans)
    end
end

# following functions allows to access DCIMG like an Array
Base.size(D::Dcimg) = (D.x, D.y, D.z, D.t)
Base.getindex(D::Dcimg, i::Int) =
    D.m[i + (i ÷ (D.x*D.y))*D.clock÷sizeof(UInt16)] # skip clock
Base.getindex(D::Dcimg, x::Int, y::Int, z::Int, t::Int) = # manual conversion to linear index
    D[x + D.x*((y-1) + D.y*((z-1) + D.z*(t-1)))]
# get frame number n (starting form 1)
getframe(D::Dcimg,n::Int)::Array{UInt16,2}  =
    reshape(D.m[
        D.x*D.y*(n-1)+1 + (n-1)*D.clock÷sizeof(UInt16) : D.x*D.y*n + (n-1)*D.clock÷sizeof(UInt16)
        ], D.x, D.y)
# get frame for layer z, time t (starting from 1)
getframe(D::Dcimg,z::Int,t::Int)::Array{UInt16,2} =
    getframe(D::Dcimg, z + D.z*(t-1))

function getRAframe(D::Dcimg,z::Int,t::Int) # like getframe, but return in good orientation
    return D.ftrans(getframe(D, D.ztrans(z), t))
end


# allowing to automatically parse size (future version)
function Dcimg_future(file::String)
    pathtag = replace(file, r"\.\w+$" => "") # remove extension
    p = TOML.parsefile(pathtag * ".toml")
    return Dcimg(pathtag * ".dcimg",
        p["meta"]["header"],
        p["meta"]["clock"],
        p["meta"]["space"],
        p["dims"]["x"],
        p["dims"]["y"],
        p["dims"]["z"],
        p["dims"]["t"],
        )
end

# according to current matlab format
function Dcimg(file::String)
    pathtag = replace(file, r"\.\w+$" => "") # remove extension
    path = replace(pathtag, r"\w+$" => "") # remove filename
    p = TOML.parsefile(path * "info.toml")
    space = p["meta"]["space"]
    # TODO when this strange behavior disappears from matlab, it should also
    # disappear from here (dimension are given in RAST space, not data space)
    if place('x', split(uppercase(space), "")) == 2
        x = p["size"]["y"]
        y = p["size"]["x"]
    else
        x = p["size"]["x"]
        y = p["size"]["y"]
    end

    return Dcimg(pathtag * ".dcimg",
        p["byte"]["header"],
        p["byte"]["clock"],
        space,
        x,
        y,
        p["size"]["z"],
        p["size"]["t"],
        )
end

export Dcimg, getRAframe

end
