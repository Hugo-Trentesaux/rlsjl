module requesthandler

using HTTP
using JSON
using dcimg
using stack
using requesthandler
using wsviewer

include("./serializers.jl")
include("./web_layer.jl")

export wshandler, TEXTresponse, JSONresponse, filebrowser, Dcimg, Stack, serialize

# ===========================================
# === routes ===

router = Dict(
    "/dcimg/" => Dcimg,
    "/stack/" => Stack,
)

function getObject(message::HTTP.Messages.Message)
    uri = HTTP.URI(message.target)
    type = router[uri.path]
    return type(message)
end

# ===========================================
# === utils ===

# to return JSON
function JSONresponse(body::String)
    header = Dict(
        "Content-Type"=>"application/json",
        "Access-Control-Allow-Origin"=>"*",
        )
    return HTTP.Response(200, header; body=body)
end

# to return TEXT file
function TEXTresponse(txt::String, type::String)
    header = Dict(
        "Content-Type"=>"text/" * type, # e.g. html, css, js
        "Access-Control-Allow-Origin"=>"*",
        )
    return HTTP.Response(200, header; body=txt)
end

# ===========================================
# === stream handlers ===

# handle stream for getting websocket
function wshandler(http::HTTP.Stream)
    obj = getObject(http.message)
    # if the http stream can be upgraded to a websocket...
    if HTTP.WebSockets.is_upgrade(http.message)
        # ... upgrade it and applies the following function
        HTTP.WebSockets.upgrade(http, binary=true) do ws::HTTP.WebSockets.WebSocket
            println("websocket is open")
            viewer(ws, obj)
        end
    else
        println("http can not be upgraded")
        HTTP.setstatus(http, 404)
        startwrite(http)
        write(http, "not available")
        return
    end
end


end
