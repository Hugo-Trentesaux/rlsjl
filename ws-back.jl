# add current path
push!(LOAD_PATH, joinpath(pwd(), "modules"))

using HTTP
using JSON
using requesthandler

# === config ===
# host (not HTTP.Sockets.localhost)
host = "0.0.0.0"
# ports
port_app = 9997 # static apps
port_api = 9998 # JSON API
port_ws = 9999 # websocket

# === apps ===
# a function to get a page based on it's name
page = (path) -> (http) -> TEXTresponse(read(open(path), String), "html")
js = (path) -> (http) -> TEXTresponse(read(open(path), String), "javascript")

# define URLs serving static files
const htmlrouter = HTTP.Router()
HTTP.@register(htmlrouter, "GET", "/", page("./apps/browser.html"))
HTTP.@register(htmlrouter, "GET", "/dcimg/", page("./apps/ws-front.html"))
HTTP.@register(htmlrouter, "GET", "/stack/", page("./apps/stack_viewer.html"))
HTTP.@register(htmlrouter, "GET", "/plotd3.js", js("./apps/plotd3.js"))
HTTP.@register(htmlrouter, "GET", "/plotchart.js", js("./apps/plotchart.js"))

# === api ===
# a function to get a JSON serialized object based on it's type
obj = (type) -> (http) -> JSONresponse(JSON.json(serialize(type(http))))

# define API endpoints
const jsonrouter = HTTP.Router()
HTTP.@register(jsonrouter, "GET", "/dcimg/", obj(Dcimg))
HTTP.@register(jsonrouter, "GET", "/stack/", obj(Stack))
HTTP.@register(jsonrouter, "GET", "/browse/", filebrowser)

# === servers ===
@async HTTP.serve(htmlrouter, host, port_app) # serves the app
@async HTTP.serve(jsonrouter, host, port_api) # start data server
@async HTTP.serve(wshandler, host, port_ws; stream=true) # websocket server

println("started servers")
