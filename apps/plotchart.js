function plotchart(data) {
    ctx = document.getElementById('chart').getContext('2d');

    chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'scatter',

        // The data for our dataset
        data: {
            labels: data,
            datasets: [{
                label: 'Neuron 1',
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: 'rgba(0, 0, 0, 0)',
                lineTension: 0,
                pointRadius: 0,
                data: data,
                showLine: true,
            }]
        },

        // Configuration options go here
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        maxTicksLimit: 10,
                        suggestedMin: 0,
                        suggestedMax: 1000,
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        suggestedMin: 400,
                        suggestedMax: 800,
                    }
                }]
            },
            tooltips: {
                enabled: false,
            },
        }
    });
}
