function plotd3(data) {
    // make this object available for other functions
    self = this

    this.root = d3.select("#plot")
    this.data = data // a list of x/y points
    this.layout = {}
    this.param = {
        width: 512,
        height: 512,
    }

    // =========================================================================
    //                                                                      INIT
    // initialize DOM elements and force render elements
    this.initComponents = function() {
        // svg is where graph will be rendered
        this.layout.svg = this.root
            .append("div")
                .attr("id", "plotframe")
            .append("svg:svg")
                .attr("width", this.param.width)
                .attr("height", this.param.height)
                .attr("pointer-events", "all");

        // background
        this.layout.svg.append('svg:rect')
                .attr('width', this.param.width)
                .attr('height', this.param.height)
                .attr('class', 'graph-background')
                .attr('fill', 'white');

        // define arrow markers for graph links
        this.layout.svg.append('svg:defs')
              .append('svg:marker')
                .attr('id', 'arrow')
                .attr('viewBox', '0 -5 10 10')
              .append('svg:path')
                .attr('d', 'M0,-5L10,0L0,5')
                .attr('fill', '#000');

        // plotLayer is the svg group where the points and edges will live
        this.layout.plot = this.layout.svg
            .append('svg:g');

        // a svg group for points
        this.layout.points = this.layout.plot
            .append("g")
                .attr("stroke", "#fff")
                .attr("stroke-width", 0.2);
        this.point = this.layout.points.selectAll("circle");

        // zoom behavior
        var zoom_handler = d3.zoom().on("zoom", zoom_actions);
        function zoom_actions(){ self.layout.plot.attr("transform", d3.event.transform); }
        zoom_handler(this.layout.svg);

    }

    // =========================================================================
    //                                                                    REDRAW
    // redraw the graph (if an element was added or deleted)
    this.redraw = function() {
        // ======================================================== NODES
        // select all circles and join data
        this.point = this.layout.points
            .selectAll("circle")
            .data(this.data, d => d); // this.point has .exit and .enter functions

        // for leaving nodes
        this.point
            .exit().transition().attr("r", 0).remove();

        this.point = this.point // updates this.node with new nodes
            .enter()
                .append("circle")
                    .attr("r", 3)
                    .attr("class", "node")
                    .attr("cx", d => d.x)
                    .attr("cy", d => d.y)
            .merge(this.point)
    }

    this.initComponents()
    this.redraw()

}
