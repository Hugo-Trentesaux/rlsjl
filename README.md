# RLSjl

RLSjl is an experiment. The first objective is to provide an easy way to access huge raster datasets. It is achieved using Julia memory mapping and websocket server for the backend and vuejs for the frontend.

The user only has to start the server on the host and access the files from his webbrowser.

In a second time, it should provide a more complete way to browse the data in different slice directions.

**install**

Install Julia 1.1, and add the required packages.
Download the code.
Run the ws-back.jl file.
Visit localhost:9997

![file explorer](./img/file_explorer.png)

> a clickable file explorer that links to the viewers

![stack viewer](./img/web_stack_viewer.png)

> an interactive stack viewer to explore huge datasets (~100GB is not a problem)
